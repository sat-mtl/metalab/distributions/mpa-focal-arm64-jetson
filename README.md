# mpa-focal-arm64-jetson 

metalab package archive (MPA) repository for ubuntu focal 20.04 arm64 jetson xavier and orin boards (not nano)

Fork of https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo

List of packages: https://sat-mtl.gitlab.io/distribution/mpa-focal-arm64-jetson/debs/dists/sat-metalab/main/binary-arm64/Packages

## Requirements

This repository requires a NVIDIA Jetson Xavier NX board running Jetson NX Developer Kit SD Card Image 5.0.2+ from https://developer.nvidia.com/embedded/downloads

## Installation

In a terminal:

```sh
sudo apt install -y coreutils software-properties-common wget && \
wget -qO - https://sat-mtl.gitlab.io/distribution/mpa-focal-arm64-jetson/sat-metalab-mpa-keyring.gpg \
    | gpg --dearmor \
    | sudo dd of=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg && \
echo 'deb [ signed-by=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg ] https://sat-mtl.gitlab.io/distribution/mpa-focal-arm64-jetson/debs/ sat-metalab main' \
    | sudo tee /etc/apt/sources.list.d/sat-metalab-mpa.list && \
echo 'deb [ arch=amd64, signed-by=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg ] https://sat-mtl.gitlab.io/distribution/mpa-datasets/debs/ sat-metalab main' \
    | sudo tee /etc/apt/sources.list.d/sat-metalab-mpa-datasets.list && \
sudo apt update
```

## Automating new packages

- First package your software for Ubuntu: https://packaging.ubuntu.com
- Add user [sat-metalab-mpa](https://gitlab.com/users/sat-metalab-mpa) as Maintainer to the repository of your software (already also added to this mpa), this is required for using the gitlab API with their access token.
- Add the following CI variable to the repository of your software:
   - key: ACCESS_TOKEN
   - value: get from existing repos, for example: https://gitlab.com/sat-mtl/tools/forks/filterpy/-/settings/ci_cd
- In your software repository, `debian/*` branch, append to `.gitlab-ci.yml`:
```
include:
  - remote: 'https://gitlab.com/sat-mtl/distribution/mpa-focal-arm64-jetson/-/raw/main/.gitlab-ci-package.yml'
```
- In [updaterepos.sh](updaterepos.sh), append the path with namespace of your software repository to variable `projects`, commit, and open a merge request.
